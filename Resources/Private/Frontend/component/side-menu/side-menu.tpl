<!--
 * side-menu.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="side-menu">
	<div class="side-menu__nav d-flex justify-content-end">
		<nav class="nav flex-column">
			<ul>
				<li>
					<a class="nav-link side-menu__link" href="#">Startseite</a>
				</li>
				<li>
					<a class="nav-link side-menu__link" href="#">Academy</a>
				</li>
				<li>
					<a class="nav-link side-menu__link" href="#">Business-Programm</a>
				</li>
				<li>
					<a class="nav-link side-menu__link" href="#">Hilfe / Kontakt</a>
				</li>
			</ul>

		</nav>
	</div>

</div>
