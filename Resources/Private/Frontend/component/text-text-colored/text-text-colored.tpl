<!--
 * text-text-colored.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="text-text-colored d-flex justify-content-center">
	<div class=" row container">
		<div class=" col-lg-4 offset-lg-1 col-md-12 offset-md-0">

			<div class="text-text-colored__text">
				<h2><span>Herkömmliche Analyse ohne DNA</span>
					Hausazrt</h2>
				<p class="text-explain"><strong>Herkömmlichen Methoden</strong> können <strong>nur ca. 15-20%</strong>  der Darmbakterien analysieren. Die Analyse der Stuhlprobe erfolgt mit einer <strong>langwierigen Kultivierungsmethode</strong> . Der Stuhl wird auf einem Nährboden ausgestrichen, um
					das Bakterienwachstum zu beobachten. Als Ergebnis erhältst du die Aussage, ob <strong>wenige wichtige</strong> und kultivierbare <strong>Stämme</strong> in deiner Darmflora
					vorhanden sind.</p>

			</div>

		</div>
		<div class=" col-lg-5 offset-lg-2 col-md-12 offset-md-0 background-orange ">
			<div class="text-text-colored__text">
				<h2 class="h2--light"><span>Biomes Analyse bakterieller DNA</span>
					INTEST.pro</h2>
				<p class="text-explain p-light">
					<strong> INTEST.pro </strong> identifiziert damit
					<strong>nahezu </strong>100% aller bekannten Darmbakterien. Die Analyse der mikrobiellen DNA erfolgt mit der <strong>modernsten biotechnischen</strong>  Next-Generation-Sequencing-Methode. Auch <strong>Bakterien</strong>  haben <strong>DNA</strong> und diese ist bereits in kleinsten
					Mengen Stuhl enthalten. Deine individuelle DNA wird mithilfe der BIOMES-<strong>Wissensdatenbank</strong> interpretiert, die die Ergebnisse von über <strong>6.000</strong>  wissenschaftlichen und klinischen <strong>Studien</strong>  zur Darmflora enthält. Als Ergebnis erhältst du
					Zugang zu einem umfangreichen <strong> Online-Dashboard</strong> mit detaillierten <strong>Erkenntnissen</strong>, <strong>Empfehlungen</strong> und einem <strong>Ernährungsplan</strong>.
				</p>
			</div>
		</div>

	</div>

</div>
