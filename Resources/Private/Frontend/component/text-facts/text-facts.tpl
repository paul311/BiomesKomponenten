<!--
 * text-facts.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-03
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="text-facts">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-10 col-lg-8">
				<h2><span>Partner*in von BIOMES</span>
					INTEST.pro ist unser Produkt zur Analyse der Darmflora</h2>
				<p class="text-lead">Mit unserer Expertise haben wir den Darmtest INTEST.pro entwickelt. Im Ergebnis erhalten Kund*innen ihr individuelles Mikrobiom-Profil inklusive Stärken, Schwachstellen und wirkungsvollen Empfehlungen. </p>
				<p>Die Analyse der Stuhlproben erfolgt in unserem eigenen Labor in Wildau bei Berlin. Zugang zu den Resultaten und Empfehlungen erhalten Kund*innen online über das BIOMES Dashboard. Eine persönliche Einwilligung vorausgesetzt, erhalten Sie, als unser*e Partner*in, den exklusiven Zugang zu den Mikrobiom-Profilen Ihrer Klient*innen. Auf dieser Grundlage können Sie gezielt beraten und unsere Empfehlungen nutzen. Als einziges Biotechnologieunternehmen europaweit bietet BIOMES,je nach individueller Schwachstelle, zusätzlich probiotische Nahrungsergänzungsmittel an, um beispielsweise Dysbalancen auszugeichen und die Darmflora schnell wiederherzustellen.</p>
				<h3 class="text-facts__header">Was macht uns besonders?</h3>
				<p>Mit unserer DNA-basierten Methode gelingt es uns, nahezu die gesamte Darmflora zu analysieren. Bei der Auswertung arbeiten wir 100% evidenzbasiert. Unsere Interpretationen beruhen immer auf dem aktuellen Stand der Wissenschaft. Dazu nutzen wir unsere BIOMES-interne Wissensdatenbank, die Ergebnisse von weltweit mehr als 7.000 wissenschaftlichen und klinischen Mikrobiomstudien zusammenfasst und nahezu täglich von unserem Team renommierter Wissenschaftler*innen aktualisiert wird.</p>
			</div>
		</div>
		<div class="text-facts__facts">
			<div class="row justify-content-center">
				<div class="col-6 col-md-3 col-lg-2">
					<div class="text-facts__fact">
						<div class="text-facts__fact-hex">
							<svg class="text-facts__fact-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 190 170.322">
								<path d="M131.72 0H58.28a21.56 21.56 0 00-18.671 10.78l-36.72 63.6a21.56 21.56 0 000 21.56l36.72 63.6a21.56 21.56 0 0018.671 10.78h73.44a21.56 21.56 0 0018.671-10.78l36.72-63.6a21.56 21.56 0 000-21.56l-36.72-63.6A21.56 21.56 0 00131.72 0z" fill="#fff"/>
							</svg>
							<div class="text-facts__fact-value">7.000</div>
						</div>
						<div class="text-facts__fact-key">wissenschaftliche <br>
							Studien</div>
					</div>
				</div>
				<div class="col-6 col-md-3 col-lg-2">
					<div class="text-facts__fact">
						<div class="text-facts__fact-hex">
							<svg class="text-facts__fact-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 190 170.322">
								<path d="M131.72 0H58.28a21.56 21.56 0 00-18.671 10.78l-36.72 63.6a21.56 21.56 0 000 21.56l36.72 63.6a21.56 21.56 0 0018.671 10.78h73.44a21.56 21.56 0 0018.671-10.78l36.72-63.6a21.56 21.56 0 000-21.56l-36.72-63.6A21.56 21.56 0 00131.72 0z" fill="#fff"/>
							</svg>
							<div class="text-facts__fact-value">20.000</div>
						</div>
						<div class="text-facts__fact-key">wissenschaftliche <br>
							Studien</div>
					</div>
				</div>
				<div class="col-6 col-md-3 col-lg-2">
					<div class="text-facts__fact">
						<div class="text-facts__fact-hex">
							<svg class="text-facts__fact-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 190 170.322">
								<path d="M131.72 0H58.28a21.56 21.56 0 00-18.671 10.78l-36.72 63.6a21.56 21.56 0 000 21.56l36.72 63.6a21.56 21.56 0 0018.671 10.78h73.44a21.56 21.56 0 0018.671-10.78l36.72-63.6a21.56 21.56 0 000-21.56l-36.72-63.6A21.56 21.56 0 00131.72 0z" fill="#fff"/>
							</svg>
							<div class="text-facts__fact-value">100%</div>
						</div>
						<div class="text-facts__fact-key">wissenschaftliche <br>
							Studien</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
