<!--
 * picture-text.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-04
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="picture-text">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-0 offset-md-1 col-md-11 offset-xs-1 col-xs-11">
				<figure class="">
					<img src="Resources/Public/img/picture-text__img.jpg" alt="Alles auf einem Blick">
				</figure>
			</div>
			<div class="col-lg-6 offset-lg-0 offset-md-1 col-md-11 offset-xs-1 col-xs-11">
				<div class="">
					<h2><span>Musterauswertung</span>
						Alles auf einen Blick.</h2>
					<p class="text-lead">Unser Dashboard zeigt auf einen Blick, wo Schwachstellen in der Darmflora bestehen und was Sie gemeinsam mit Ihren Klient*innen tun können, um sie wieder ins Gleichgewicht zu bringen. Es ist der Einstieg in eine spannende Reise zu mehr Gesundheit, Leistungsfähigkeit und Lebensqualität. </p>
					<p><a class="button--glitch button">Selber testen</a> <a class="button button--ghost">Musterauswertung</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
