<!--
 * email-download.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-18
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="email-download">
	<div class="">
		<div class="email-download__grey">

		</div>
		<div class="email-download__text">

			<p class="text-mini email-download__type">PDF</p>
			<h5 class="email-download__header">Fragenkatalog</h5>
			<p class="">Fließtext – Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>


			<div class="d-flex items-align-center">

				<svg id="Component_12_1" data-name="Component 12 – 1" xmlns="http://www.w3.org/2000/svg" width="35" height="30.282" viewBox="0 0 35 30.282">
		  <path id="Polygon_288" data-name="Polygon 288" d="M23.365,0a5,5,0,0,1,4.329,2.5l5.861,10.141a5,5,0,0,1,0,5L27.694,27.784a5,5,0,0,1-4.329,2.5H11.635a5,5,0,0,1-4.329-2.5L1.446,17.643a5,5,0,0,1,0-5L7.306,2.5A5,5,0,0,1,11.635,0Z" fill="#ff7719"/>
		  <text id="_" data-name="" transform="translate(17.5 21.141)" fill="#fff" font-size="16" font-family="FontAwesome5FreeSolid, 'Font Awesome \35  Free'"><tspan x="-6" y="0"></tspan></text>
		</svg>
		




				<button class="btn email-download__download" type="button" name="button">
					<h6 class="text-mini ">herunterladen</h6>
				</button>

			</div>
		</div>


	</div>
</div>
