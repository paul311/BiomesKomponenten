<!-- footer:start -->
<footer class="footer">
	<div class="footer__top">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-4 col-lg-3">
					<div class="footer__nav">
						<div class="footer__nav-header">BIOMES Business-Portal</div>
						<a href="#" class="footer__nav-link footer__nav-link--sub">KMU A</a>
						<a href="#" class="footer__nav-link footer__nav-link--sub">KMU B</a>
						<!-- <a href="#" class="footer__nav-link footer__nav-link--sub">Affiliate</a>
						<a href="#" class="footer__nav-link footer__nav-link--sub">Projektplaner</a> -->
					</div>
				</div>
				<div class="col-6 col-md-4 col-lg-3">
					<div class="footer__nav">
						<a href="#" class="footer__nav-link">Kontakt</a>
						<a href="#" class="footer__nav-link">Presse</a>
						<a href="#" class="footer__nav-link">FAQ</a>
						<!-- <a href="#" class="footer__nav-link">e-Books</a>
						<a href="#" class="footer__nav-link">Newsletter</a> -->
					</div>
				</div>
				<div class="col-6 col-lg-3 offset-lg-3">
					<img src="Resources/Public/img/payment.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="footer__mid">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="footer__service">
						<a class="footer__service-link" href="#">Impressum</a>
						<a class="footer__service-link" href="#">Datenschutz</a>
						<a class="footer__service-link" href="#">AGB</a>
						<a class="footer__service-link" href="#">Zahlungsarten & Versand</a>
						<a class="footer__service-link" href="#">Zweckbestimmungen</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer__copyright">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="footer__colophon">© BIOMES NGS GmbH — Made in Germany with ♥</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- footer:end -->
