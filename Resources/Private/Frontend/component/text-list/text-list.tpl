<!--
 * text-list.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-05
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="text-list d-flex justify-content-center align-items-center">
	<!-- <img src="Resources/Public/img/corner-right-pop.svg" alt="corner"> -->
	<div class="container row d-flex justify-content-center">
		<div class=" margin-element col-12 col-md-10 col-lg-8 ">
			<h2 class="h2">
				<span>Ihre Vorteile</span>
				Flexbile Konditionen für ihre Bedürfnisse
			</h2>
			<div class="">
				<h3>Partnerpreise
				</h3>
				<p class="text-lead">Gezielt auf Ihre Bedürfnisse abgestimmt: Nutzen Sie unsere individuellen Partnerpreise und sichern Sie sich einmalig 50% Nachlass auf ihre erste INTEST.pro Bestellung
				</p>
				<h3>Schulungsmaterial
				</h3>
				<p class="text-lead">
					Erweitern Sie Ihre Kompetenzen mit unserem umfangreichen Schulungsmaterial

				</p>
				<h3>Marketing- und Vertriebsmaterial

				</h3>
				<p class="text-lead">
					Nehmen Sie die Vorteile eines starken Produktes wahr und ordern Sie Werbe-Material für Ihr Marketing
				</p>
				<h3>Support
				</h3>
				<p class="text-lead">
					Gemeinsam für ein starkes Gesundheitsnetzwerk: Wir sind bei allen Fragen persönlich für Sie da
				</p>
				<h3>Academy
				</h3>
				<p class="text-lead">
					Immer auf dem neuesten Stand: Bilden Sie sich in unserer Academy fort
				</p>
			</div>

			<!-- <p class="text-list__text text-lead">Mit unseren BUSINESS-PORTAL schaffen wir ein starkes Netzwerk von
				Partner*innen und unterstützen sie alle, deren Herz für die Gesundheit ihrer
				Kund*innen schlägt.</p>

			<h3 class="text-list__subheader">Vorteile</h3>
			<ul class="text-list__list">
				<li>

					Individuelle Koonditionen.
				</li>
				<li>

					Testen Sie selber kostenlos INTEST.pro.
				</li>
				<li>
			Bilden Sie sich in der Academy fort.
				</li>
				<li>

					Umfangreiches Promotion-Material.
				</li>
			</ul> -->
			<a class="button button--glitch" type="" name="button">
				<h9>Jetzt Partner*in werden</h9>
			</a>
		</div>
	</div>

</div>
