<!--
 * text-heaxgons.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="text-hexagons">
	<div class="container">
		<div class="row">
			<div class="offset-1 col-lg-7 col-md-10">
				<h2><span>BIOMES Partnerschaft</span>
					Wir wollen Menschen unterstützen gesünder und fitter zu werden. </h2>

				<p>
					Dazu haben wir die derzeit modernste Methode zur Analyse der Darmmikrobiom-DNA (NGS) entwickelt. Mit diesem Verfahren identifizieren und interpretieren den individuellen mikrobiellen Fingerabdruck jeder Probe und ermöglichen so genaueste Beratungsmöglichkeiten. Gemeinsam mit Ihnen möchten wir neue Wege gehen, damit unsere Vision von einem gesünderen Leben auch für Ihre Klient*innen Wirklichkeit wird.
				</p>
			</div>
		</div>
		<div class="">
			<div class="row justify-content-center">
				<div class="col-4 col-md-3 col-lg-2">
					<img src="Resources/Public/img/hexagon-picture-1.png" alt="picture">
					<p class="text-center text-hexagons__topic">Affiliate</p>
				</div>
				<div class="col-4 col-md-3 col-lg-2">
					<img src="Resources/Public/img/hexagon-picture-2.png" alt="picture">
					<p class="text-center text-hexagons__topic">Geschäftspartner</p>
				</div>
				<div class="col-4 col-md-3 col-lg-2">
					<img src="Resources/Public/img/hexagon-picture-3.png" alt="picture">
					<p class="text-center text-hexagons__topic">Gesundheitsberater</p>
				</div>
				<div class="col-4 col-md-3 col-lg-2">
					<img src="Resources/Public/img/hexagon-picture-4.png" alt="picture">
					<p class="text-center text-hexagons__topic">BGM</p>
				</div>
				<div class="col-4 col-md-3 col-lg-2">
					<img src="Resources/Public/img/hexagon-picture-1.png" alt="picture">
					<p class="text-center text-hexagons__topic">Forschungs-service</p>
				</div>

			</div>
		</div>
	</div>
</div>
