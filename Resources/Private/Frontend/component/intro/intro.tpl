<!--
 * intro.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-08
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="intro">
	<div class="intro__background">
		<figure class="intro__figure ">
			<img class="intro__img" src="Resources/Public/img/menschen_lachen.jpg" alt="Menschen Lachen">
		</figure>
	</div>
	<div class="intro__foreground intro__foreground-inner">

			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-5 offset-lg-1">
						<h1 class="h2 h2--light"><span>BIOMES Business-portal</span>
							Für ein gesünderes Leben.</h1>
						<p class="text-lead p-light">Das ist unsere Vision: Gemeinsam mit Ihnen wollen wir Menschen gesünder,
							fitter und leistungsfähiger machen.</p>
						<p><a href="#" class="button button--glitch ">Informieren Sie sich</a></p>
					</div>
				</div>

		</div>
	</div>
</div>
