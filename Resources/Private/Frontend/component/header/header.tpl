<!-- header:start -->
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col">
				<a href="#" class="header__branding">
					<div class="header__branding-svg">
						<svg id="kreis" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1417.32 1417.32" width="33" height="33">
							<defs>
								<clipPath id="clip-path">
									<path class="cls-1" d="M433.59-632.5h697.81V65.31H433.59z"/>
								</clipPath>
								<clipPath id="clip-path-2">
									<path class="cls-1" d="M-268.32-268.32h1953.97v1953.97H-268.32z"/>
								</clipPath>
								<clipPath id="clip-path-3">
									<path class="cls-1" d="M-268.32-268.33h1953.97v1953.97H-268.32z"/>
								</clipPath>
								<clipPath id="clip-path-4">
									<path class="cls-1" d="M-268.32 1177.35h1953.97v1953.97H-268.32z"/>
								</clipPath>
								<style>
									.cls-1{fill:none}.cls-6{fill:#31ae99}
								</style>
							</defs>
							<g clip-path="url(#clip-path-2)">
								<g clip-path="url(#clip-path-3)">
									<path fill="#385953" d="M384.56 84.6h.01-.01"/>
									<path class="cls-6" d="M570.29 940.47h30.24-30.24M709.3 4.09a707.41 707.41 0 00-150.44 16.1l1.21 1.1C597.36 54.63 750.59 165.72 828 234.1 1039.48 420.93 1112.5 548 1124.47 709c10.44 140.36-9.92 238.59-80.14 331 18.31-29.83 19.82-64.39 19.82-100.89H558.09c50.8 54.09 358.54 311.31 475.3 393.51l.74.53c178.33-92.9 311.91-259.88 359.92-460.14q.6-2.46 1.18-4.92l.33-1.42a707.46 707.46 0 0017.88-155.25V711v-2.78c0-388.89-315.27-704.16-704.16-704.16"/>
									<path d="M954.06 586.35C920.58 539 880.71 505 830.22 505h-475.1c1.27 43.35 11.71 108.7 22.68 142 9.58 29.07 21.07 48.89 57.81 48.84q69.47-.06 139.6-.08h133.47q9.47 0 19.21.19h114.24q70.25 0 139.6-.09c16 0 27.2 3.73 35.55 10.37a656.92 656.92 0 00-63.22-119.88" fill="#e37b3a"/>
									<path class="cls-6" d="M709.15 3.76a706.84 706.84 0 00-150.44 16.11l1.22 1.1c37.29 33.33 190.51 144.42 267.92 212.8 211.48 186.83 284.51 313.91 296.48 474.91 10.43 140.36-9.93 238.58-80.14 331 18.26-29.77 19.81-64.24 19.82-100.64H590.59a106.55 106.55 0 01-41.76-8.66c50.8 54.08 367.65 319.72 484.41 401.91l.75.52C1212.32 1240 1345.9 1073 1393.92 872.72c.4-1.64.78-3.28 1.17-4.92l.33-1.42a706.93 706.93 0 0017.88-155.25v-.43-2.77c0-388.9-315.27-704.17-704.17-704.17"/>
									<path class="cls-6" d="M384.09 84.28l-.74-.53C205 176.64 71.43 343.62 23.41 543.88q-.6 2.46-1.17 4.92c-.11.47-.22.93-.33 1.42A706.86 706.86 0 004 705.47v3.19c0 388.9 315.27 704.17 704.17 704.17a707.32 707.32 0 00150.43-16.1l-1.21-1.11c-37.29-33.32-190.51-144.41-267.92-212.79C378 996 305 868.9 293 707.91c-10.44-140.36 9.93-238.59 80.14-331-18.27 29.76-19.82 64.23-19.82 100.63h473.42a106.59 106.59 0 0141.76 8.66c-50.81-54.09-367.66-319.73-484.41-401.92"/>
									<path d="M463.28 830.9c33.48 47.32 73.34 81.35 123.84 81.35h475.09c-1.27-43.36-11.7-108.71-22.68-142-9.58-29.08-21.06-48.89-57.8-48.85q-69.47.06-139.6.08H708.66q-9.46 0-19.21-.19H575.21q-70.25 0-139.6.08c-16 0-27.2-3.73-35.55-10.36a656.83 656.83 0 0063.22 119.89" fill="#4c569c"/>
								</g>
							</g>
						</svg>
					</div>
					<div class="header__branding-text">Business-Portal</div>
				</a>
			</div>
		</div>
	</div>
</header>
<!-- header:end -->
