<!--
 * text-orange.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="text-orange">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-10 col-lg-8">
				<h2 class="h2--light"><span>Verifizierung</span>
					Fast fertig</h2>
				<p class="text-lead p-light">
					Willkommen auf dem BIOMES Business-Portal. <br> <br>

					Sie haben Ihren Registrierungsprozess jetzt abgeschlossen.
					Um einen uneingeschränkten Zugang zu unseren Inhalten und Produkten zu erhalten, müssen Sie nur noch Ihr Profil vervollständigen. </p>

					<p><a href="#" class=" button button--glitch">Vorteile Partnerschaft</a></p>



			</div>
		</div>
	</div>
</div>
