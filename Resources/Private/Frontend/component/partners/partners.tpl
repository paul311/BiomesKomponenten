<!--
 * partners.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-08
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="partners container d-flex justify-content-center">
	<div class="row">
		<h3 class="offset-1 partners__subheader">Unsere wissenschaftlichen Partner</h3>
		<div class="">
			<img class="partners__img col-lg-2 col-md-3 col-sm-4 col-4 offset-1" src="Resources/Public/img/carl-thiem-klinikum.jpg" alt="Carl Thiem Klinikum">
			<img class="partners__img col-lg-2 col-md-3 col-sm-4 col-4" src="Resources/Public/img/gorjerno-de-mexico-education.jpg" alt="Gorjerno de mexico education">
			<img class="partners__img col-lg-2 col-md-3 offset-md-0 offset-sm-1 col-4 col-sm-4"src="Resources/Public/img/charite.jpg" alt="Charite">
			<img class="partners__img col-lg-2 col-md-3 offset-md-3 offset-lg-0 col-4 col-sm-4"src="Resources/Public/img/technische-hochschule-wildau.jpg" alt="Technische Hochschule Wildau">
			<img class="partners__img col-lg-2 col-md-3 offset-1 offset-sm-3 offset-md-0 col-4 col-sm-4"src="Resources/Public/img/unviersität-wien.jpg" alt="Unviersität Wien">
		</div>
	</div>
</div>
