<!--
 * product-checkboxes.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="product-checkboxes">
	<div class="container">
		<h3>
			Finden Sie die richtigen Informationen
		</h3>
		<div class="product-checkboxes__text">
			<div class="d-flex row">
				<p class=" col-2 col-lg-1 product-checkboxes__type">
					Produkte
				</p>
				<div class=" d-flex col-sm-10 col-12">
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Intest.pro</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Intest.pro Kids</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Nahrungsergänzungsmittel</label>
					</div>
				</div>
			</div>
			<div class="d-flex row">
				<p class=" col-2 col-lg-1 product-checkboxes__type">
					Typ
				</p>
				<div class=" d-flex col-sm-10 col-12">
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Produktanleitungen</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">FAQs</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Technologie</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Wissenschaftliche Neuheiten</label>
					</div>
				</div>
			</div>
			<div class="d-flex row">
				<p class=" col-2 col-lg-1 product-checkboxes__type">
					Zielgruppe
				</p>
				<div class=" d-flex col-sm-10 col-12">
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Heilprakter*in</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Ernährungsexpert*in</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Apotheker*in</label>
					</div>
					<div class="form-check product-checkboxes__checkboxes">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label product-checkboxes__checkboxes__label" for="exampleCheck1">Trainer*in</label>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
