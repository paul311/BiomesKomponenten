<!--
 * person-small.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-18
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="person-small d-flex items-align-center">
	<div class="">
		<h4 class="person-small__header">Wir mögen direkten Kontakt</h4>
		<p class="text-explain">
			Wenn sie Fragen haben oder Hilfe brauchen - <br> wir sind für sie da. So erreichen sie uns
		</p>
		<div class="d-flex items-align-center">
			<div class="">
				<img src="/Resources/Public/img/person-small.png" alt="person">
			</div>
			<div class="d-flex justify-content-center flex-column person-small__contact">

					<h6 class="person-small__name">Andrej Wackerow</h6>
					<p class="text-explain m-0"> +49 (0) 3375 585 62 39<br>
						<a href="mailto:test@test.de">vertrieb@biomes.world</a></p>

			</div>
		</div>
		<hr>
		<p class="text-explain">Erster Antowrten und die häufigsten Fragen finden Sie auf unserer Hilfe und Kontakt-Seite</p>
			<p><a href="#" class="button button--glitch"><span>Hilfe und Kontakt</span></a></p>
	</div>

</div>
