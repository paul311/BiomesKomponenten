<!--
 * testimonials.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-08
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="testimonials">
	<figure class="testimonials__background">
		<img src="Resources/Public/img/neun-background.jpg">
	</figure>
	<div class="testimonials__container">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10">
					<h2><span>Gemeinsam Erfolge feiern</span>
						Das sagen unsere Partner*innen</h2>
				</div>
			</div>
			<div class="testimonials__cards d-none d-lg-flex">
				<div class="row">
						<div class="testimonials__card col-12 col-md-4">
							<div class="testimonials__card-inner">
								<div class="testimonials__card-main">
									<p>Unser Darm ist die Wiege der Gesundheit. Deshalb ist es wichtig, dass wir auf ihn achten und ihn in seinen vielen Funktionen unterstützen. Eure Analyse hilft mir sehr dabei, wenn ich schaue, wie es um die Gesundheit meiner Klienten steht. Sie ist die Grundlage von gemeinsamen Entscheidungen für ein gesünderes Leben.</p>
								</div>
								<div class="testimonials__card-footer">
									<div class="row">
										<div class="col-3">
											<img src="Resources/Public/img/barbara-winkler.jpg" alt="">
										</div>
										<div class="col-9">
											<div class="testimonials__person">
												<div class="testimonials__person-name">Barbara Winkler</div>
												<div class="testimonials__person-title">Physiotherapeutin</div>
												<div class="testimonials__person-company">Barbara Winkler NaturheilPraxis</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="testimonials__card col-12 col-md-4">
							<div class="testimonials__card-inner">
								<div class="testimonials__card-main">
									<p>Unser Darm ist die Wiege der Gesundheit. Deshalb ist es wichtig, dass wir auf ihn achten und ihn in seinen vielen Funktionen unterstützen. Eure Analyse hilft mir sehr dabei, wenn ich schaue, wie es um die Gesundheit meiner Klienten steht. Sie ist die Grundlage von gemeinsamen Entscheidungen für ein gesünderes Leben.</p>
								</div>
								<div class="testimonials__card-footer">
									<div class="row">
										<div class="col-3">
											<img src="Resources/Public/img/barbara-winkler.jpg" alt="">
										</div>
										<div class="col-9">
											<div class="testimonials__person">
												<div class="testimonials__person-name">Barbara Winkler</div>
												<div class="testimonials__person-title">Physiotherapeutin</div>
												<div class="testimonials__person-company">Barbara Winkler NaturheilPraxis</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="testimonials__card col-12 col-md-4">
							<div class="testimonials__card-inner">
								<div class="testimonials__card-main">
									<p>Unser Darm ist die Wiege der Gesundheit. Deshalb ist es wichtig, dass wir auf ihn achten und ihn in seinen vielen Funktionen unterstützen. Eure Analyse hilft mir sehr dabei, wenn ich schaue, wie es um die Gesundheit meiner Klienten steht. Sie ist die Grundlage von gemeinsamen Entscheidungen für ein gesünderes Leben.</p>
								</div>
								<div class="testimonials__card-footer">
									<div class="row">
										<div class="col-3">
											<img src="Resources/Public/img/barbara-winkler.jpg" alt="">
										</div>
										<div class="col-9">
											<div class="testimonials__person">
												<div class="testimonials__person-name">Barbara Winkler</div>
												<div class="testimonials__person-title">Physiotherapeutin</div>
												<div class="testimonials__person-company">Barbara Winkler NaturheilPraxis</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div id="carouselExampleIndicators" class="carousel slide d-flex d-lg-none" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  </ol>
		  <div class="carousel-inner">
		    <div class="carousel-item active">
					<div class="testimonials__card col-12">
						<div class="testimonials__card-inner">
							<div class="testimonials__card-main">
								<p>Unser Darm ist die Wiege der Gesundheit. Deshalb ist es wichtig, dass wir auf ihn achten und ihn in seinen vielen Funktionen unterstützen. Eure Analyse hilft mir sehr dabei, wenn ich schaue, wie es um die Gesundheit meiner Klienten steht. Sie ist die Grundlage von gemeinsamen Entscheidungen für ein gesünderes Leben.</p>
							</div>
							<div class="testimonials__card-footer">
								<div class="row">
									<div class="col-md-2 col-3">
										<img src="Resources/Public/img/barbara-winkler.jpg" alt="">
									</div>
									<div class="col-8 offset-1">
										<div class="testimonials__person">
											<div class="testimonials__person-name">Barbara Winkler</div>
											<div class="testimonials__person-title">Physiotherapeutin</div>
											<div class="testimonials__person-company">Barbara Winkler NaturheilPraxis</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
		    </div>
		    <div class="carousel-item">
					<div class="testimonials__card col-12">
 					 <div class="testimonials__card-inner">
 						 <div class="testimonials__card-main">
 							 <p>Unser Darm ist die Wiege der Gesundheit. Deshalb ist es wichtig, dass wir auf ihn achten und ihn in seinen vielen Funktionen unterstützen. Eure Analyse hilft mir sehr dabei, wenn ich schaue, wie es um die Gesundheit meiner Klienten steht. Sie ist die Grundlage von gemeinsamen Entscheidungen für ein gesünderes Leben.</p>
 						 </div>
 						 <div class="testimonials__card-footer">
 							 <div class="row">
 								 <div class="col-md-2 col-3">
 									 <img src="Resources/Public/img/barbara-winkler.jpg" alt="">
 								 </div>
 								 <div class="col-8 offset-1">
 									 <div class="testimonials__person">
 										 <div class="testimonials__person-name">Barbara Winkler</div>
 										 <div class="testimonials__person-title">Physiotherapeutin</div>
 										 <div class="testimonials__person-company">Barbara Winkler NaturheilPraxis</div>
 									 </div>
 								 </div>
 							 </div>
 						 </div>
 					 </div>
 				 </div>
		    </div>
		    <div class="carousel-item">
					<div class="testimonials__card col-12">
						<div class="testimonials__card-inner">
							<div class="testimonials__card-main">
								<p>Unser Darm ist die Wiege der Gesundheit. Deshalb ist es wichtig, dass wir auf ihn achten und ihn in seinen vielen Funktionen unterstützen. Eure Analyse hilft mir sehr dabei, wenn ich schaue, wie es um die Gesundheit meiner Klienten steht. Sie ist die Grundlage von gemeinsamen Entscheidungen für ein gesünderes Leben.</p>
							</div>
							<div class="testimonials__card-footer">
								<div class="row">
									<div class="col-md-2 col-3">
										<img src="Resources/Public/img/barbara-winkler.jpg" alt="">
									</div>
									<div class="col-8 offset-1">
										<div class="testimonials__person">
											<div class="testimonials__person-name">Barbara Winkler</div>
											<div class="testimonials__person-title">Physiotherapeutin</div>
											<div class="testimonials__person-company">Barbara Winkler NaturheilPraxis</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
		    </div>
		  </div>
		  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    <span class="" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    <span class="" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
		</div>
	</div>
</div>
