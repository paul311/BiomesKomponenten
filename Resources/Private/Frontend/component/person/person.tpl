<!--
 * person.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-08
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="person">
	<div class="container">
		<div class=" d-flex flex-column-reverse flex-lg-row">
			<div class="col-12 col-lg-5 align-self-lg-center">
				<div class="person__text">
					<h2 class="h2"><span>Ihre direkte Ansprechpartner*in</span>
						Andrej Wackerow</h2>
					<p class="p-bold">Wir mögen direkten Kontakt</p>
					<p>Wenn Sie Fragen haben oder Hilfe brauchen – wir sind für Sie da. So erreichen Sie uns:</p>
					<p> +49 (0) 3375 585 62 39<br>
						<a href="mailto:test@test.de">vertrieb@biomes.world</a></p>
				</div>
			</div>
			<div class="cd-12 col-lg-7">
				<figure class="person__figure d-flex justify-content-center">
					<img src="Resources/Public/img/man-with-suit.png" alt="">
				</figure>
			</div>
		</div>
	</div>
</div>
