<!--
 * text-picture.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="text-picture">
	<div class="container">
		<div class="row d-flex align-items-center">

			<div class="col-lg-5 offset-lg-0 offset-md-1 col-md-11 offset-xs-1 col-xs-11">
				<div class="">
					<h2 class="text-picture__header"><span>Sonderlösungen und große Volumina
						</span>
						Geschäftspartner*innen</h2>
						<p>
							<strong>
							Pharma Großhandel – Distributoren - Unternehmer*innen - Portfolio- und Produktmanager*innen – White Label Partner*innen

							</strong>
						</p>
					<p class="">
				Ihre Kund*innen vertrauen auf Sie und Sie möchten Ihre Produktpalette im Bereich Prävention und Digital Health ausbauen?  BIOMES unterstützt sie mit den besten DNA-Tests zur Analyse des Darmmikrobioms. Mit wissenschaftlichem Background und Biotech-Expertise haben wir schon mehr als 20.000 Kund*innen überzeugt. Vom Whitelabeling bis hin zu Sonderlösungen können wir Ihnen auch für große Volumina attraktive Konditionen anbieten. Lassen Sie uns über Ihre Möglichkeiten sprechen.
					<p><a class="button--glitch button">Informieren sie sich</a>

					</p>
				</div>
			</div>
			<div class="col-lg-6 offset-lg-1 offset-md-0 col-md-12 offset-xs-0 col-xs-11">
				<figure class="text-picture__figure col-md-12">
					<img src="Resources/Public/img/people-pharmacy.jpg" alt="Alles auf einem Blick">
				</figure>
			</div>
		</div>
	</div>
</div>
