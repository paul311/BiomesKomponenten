<!--
 * text-buttons.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-03
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->

<div class="text-buttons">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-10 col-lg-8">
				<h2><span>Partner*in von BIOMES</span>
					Gemeinsam neue Wege gehen</h2>
				<p class="text-lead">Mit unserer einzigartigen und vielfach ausgezeichneten Analyse der Darmflora möchten wir Sie – Gesundheitsexperten*innen, Trainer*innen, Therapeuten*innen, Heilpraktiker*innen und Ernährungsberater*innen - unterstützen, Ihre Klient*innen noch genauer und besser zu beraten.</p>
				<p class="text-lead">BIOMES ist Wissenschaft und Passion. Wir tun alles dafür, dass aus unserer Vision von einem gesünderen Leben Wirklichkeit wird. Mit unseren biotechnologischen und molekularbiologischen Wurzeln haben wir die derzeit modernste Methode zur Mikrobiom-DNA Analyse (NGS) entwickelt. Davon können auch Sie und Ihre Klient*innen profitieren.</p>
				<p><a href="#" class="button button--glitch">Vorteile Partnerschaft</a> <a href="#" class="button button--ghost">Partner*in werden</a></p>
			</div>
		</div>
	</div>
</div>
