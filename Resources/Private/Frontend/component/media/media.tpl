<!--
 * media.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-08
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="media">
	<div class="container">
		<div class="row">
			<div class="col-xl-7 col-lg-7 col-12">
				<div class="media__text">
					<h2><span>Methodik & Funktionsweise</span>
						Was macht BIOMES genau?</h2>
					<p class="text-lead">Die Zusammensetzung der Darmflora ist so individuell wie ein Fingerabdruck. Während herkömmliche Methoden schnell an ihre Grenzen geraten und maximal 20% der Darmbakterien identifizieren, arbeitet BIOMES mit
						der Next Generation Sequencing Methode (kurz NGS). Mit diesem Verfahren analysieren wir die DNA von (Darm-)Mikroben und identifizieren und interpretieren so den individuellen mikrobiellen Fingerabdruck jeder Probe. <br>
						Unser Laborprozess mit der NGS-Technologie ermöglicht es erstmals, nahezu 100% der im Darm befindlichen Mikroben zu analysieren. </p>
					<p><a href="#" class="button button--glitch">Jetzt Partner*in werden</a>
						<a href="#" class="button button--ghost">Mehr über die Technologie</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="media__videos position-absolute d-lg-flex d-none">
		<div class="media__videos__left">
			<a href="#"><img class="media__link" src="Resources/Public/img/bottle-link.png" alt="hexagon-link">
				<img class="d-block media__play-1" src="Resources/Public/img/play-button.png" alt="play"></a>
		</div>
		<div class="media__videos__middle">
			<a href="#"><img class="media__link" src="Resources/Public/img/bottle-link.png" alt="hexagon-link">
				<svg class="media__play-2" xmlns="http://www.w3.org/2000/svg" width="85" height="79" viewBox="0 0 85 79">
					<g id="Group_109" data-name="Group 109" transform="translate(-1227 -1401)">
						<path id="Polygon_271" data-name="Polygon 271" d="M57.774,0a10,10,0,0,1,8.807,5.262l15.87,29.5a10,10,0,0,1,0,9.475l-15.87,29.5A10,10,0,0,1,57.774,79H27.226a10,10,0,0,1-8.807-5.262l-15.87-29.5a10,10,0,0,1,0-9.475l15.87-29.5A10,10,0,0,1,27.226,0Z" transform="translate(1227 1401)" fill="#fff"/>
						<path id="Polygon_272" data-name="Polygon 272" d="M16.5,0,33,29H0Z" transform="translate(1244.961 1432.193) rotate(-30)" fill="#ff7718"/>
					</g>
				</svg>
			</a>
			<br>
			<a href="#"><img class="media__link" src="Resources/Public/img/lab-link.png" alt="hexagon-link">
				<svg class="media__play-1" xmlns="http://www.w3.org/2000/svg" width="85" height="79" viewBox="0 0 85 79">
				  <g id="Group_109" data-name="Group 109" transform="translate(-1227 -1401)">
				    <path id="Polygon_271" data-name="Polygon 271" d="M57.774,0a10,10,0,0,1,8.807,5.262l15.87,29.5a10,10,0,0,1,0,9.475l-15.87,29.5A10,10,0,0,1,57.774,79H27.226a10,10,0,0,1-8.807-5.262l-15.87-29.5a10,10,0,0,1,0-9.475l15.87-29.5A10,10,0,0,1,27.226,0Z" transform="translate(1227 1401)" fill="#fff"/>
				    <path id="Polygon_272" data-name="Polygon 272" d="M16.5,0,33,29H0Z" transform="translate(1244.961 1432.193) rotate(-30)" fill="#ff7718"/>
				  </g>
				</svg>
			</a>

		</div>
		<div class="media__videos__right ">
			<a href="#"><img class="media__link" src="Resources/Public/img/bottle-link.png" alt="hexagon-link"></a>
			<br>
			<a href="#"><img class="media__link" src="Resources/Public/img/hexagon-link.png" alt="hexagon-link"></a>
		</div>

	</div>
	<div class="d-flex justify-content-center d-lg-none media__responsive row">
		<div class="media__videos__responsive">
			<a href="#"><img class="media__link d-flex" src="Resources/Public/img/hexagon-link.png" alt="hexagon-link">
				<div class=" media__play--responsive">
					<svg class="position-absolute" xmlns="http://www.w3.org/2000/svg" width="85" height="79" viewBox="0 0 85 79">
						<g id="Group_109" data-name="Group 109" transform="translate(-1227 -1401)">
							<path id="Polygon_271" data-name="Polygon 271" d="M57.774,0a10,10,0,0,1,8.807,5.262l15.87,29.5a10,10,0,0,1,0,9.475l-15.87,29.5A10,10,0,0,1,57.774,79H27.226a10,10,0,0,1-8.807-5.262l-15.87-29.5a10,10,0,0,1,0-9.475l15.87-29.5A10,10,0,0,1,27.226,0Z" transform="translate(1227 1401)" fill="#fff"/>
							<path id="Polygon_272" data-name="Polygon 272" d="M16.5,0,33,29H0Z" transform="translate(1244.961 1432.193) rotate(-30)" fill="#ff7718"/>
						</g>
					</svg>
				</div>

			</a>
		</div>
		<div class="media__videos__responsive">
			<a href="#"><img class="media__link" src="Resources/Public/img/lab-link.png" alt="hexagon-link">
				<div class=" media__play--responsive">
					<svg class="position-absolute" xmlns="http://www.w3.org/2000/svg" width="85" height="79" viewBox="0 0 85 79">
						<g id="Group_109" data-name="Group 109" transform="translate(-1227 -1401)">
							<path id="Polygon_271" data-name="Polygon 271" d="M57.774,0a10,10,0,0,1,8.807,5.262l15.87,29.5a10,10,0,0,1,0,9.475l-15.87,29.5A10,10,0,0,1,57.774,79H27.226a10,10,0,0,1-8.807-5.262l-15.87-29.5a10,10,0,0,1,0-9.475l15.87-29.5A10,10,0,0,1,27.226,0Z" transform="translate(1227 1401)" fill="#fff"/>
							<path id="Polygon_272" data-name="Polygon 272" d="M16.5,0,33,29H0Z" transform="translate(1244.961 1432.193) rotate(-30)" fill="#ff7718"/>
						</g>
					</svg>
				</div>
			</a>
		</div>
		<div class="media__videos__responsive">
			<a href="#"><img class="media__link" src="Resources/Public/img/bottle-link.png" alt="hexagon-link"></a>
		</div>
	</div>
</div>
