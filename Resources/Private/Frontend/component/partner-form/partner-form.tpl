<!--
 * partner-form.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-05
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="partner-form d-flex justify-content-center align-items-center">
	<div class="partner-form__container container">
		<h2 class="h2">
			<span>Jetzt BIOMES Partner*in werden</span>
			Gemeinsam sind wir stärker und erreichen mehr.
		</h2>

		<form>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Unternehmensname</label>
				<input class="form-control" id="exampleInputName1" aria-describedby="emailHelp">

			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Branche</label>
				<select class="custom-select">
					<option selected></option>
					<option value="1">Branche-1</option>
					<option value="2">Branche-2</option>
					<option value="3">Branche-3</option>
				</select>

			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInputPassword1">E-Mail</label>
				<input type="email" class="form-control" id="exampleInputEmail1">
			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Anrede</label>
				<select class="custom-select">
					<option selected></option>
					<option value="1">Herr</option>
					<option value="2">Frau</option>
					<option value="3">Divers</option>
				</select>
			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Vorname</label>
				<input class="form-control" id="exampleInputName1" aria-describedby="emailHelp">

			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Nachname</label>
				<input class="form-control" id="exampleInputName2">
			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Website</label>
				<input class="form-control" id="exampleProfession1" aria-describedby="emailHelp">
			</div>
			<div class="form-group">
				<label class="text-explain" for="exampleInput">Persönliche Nachricht</label>
				<textarea class="form-control" id="exampleInputName1" aria-describedby="emailHelp"></textarea>

			</div>
			<p class="text-explain">
				Weil uns die Gesundheit unserer Kunden am Herzen liegt, sind Verantwortung und Professionalität zentrale Werte unserer Arbeit. Aus diesem Grund möchten wir sicherstellen, dass diese Werte auch von unseren Business-Partnern geteilt werden. Nicht alle Interessenten erfüllen diese Kriterien und erhalten leider keinen Zugang zum BIOMES Business-Portal.
			</p>
			<div class="row partner-form__checkboxes">
				<div class="col-9">
					<div class="form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label partner-form__label" for="exampleCheck1">Ich hab die <a class="partner-form__link" href="#">Datenschutzbestimmungen</a> gelesen und erkenne diese an.</label>
					</div>
				</div>
				<div class="col-3 partner-form__button">
					<button type="submit" class="btn">
						<svg id="Component_10_1" data-name="Component 10 – 1" xmlns="http://www.w3.org/2000/svg" width="85" height="89" viewBox="0 0 85 89">
						  <path id="Path_74" data-name="Path 74" d="M204.313,113.321H171.458a9.645,9.645,0,0,0-8.353,4.823L146.677,146.6a9.645,9.645,0,0,0,0,9.645L163.1,184.7a9.645,9.645,0,0,0,8.353,4.823h32.855a9.645,9.645,0,0,0,8.353-4.823l16.427-28.453a9.645,9.645,0,0,0,0-9.645l-16.427-28.453A9.645,9.645,0,0,0,204.313,113.321Z" transform="translate(-145.385 -103.321)" fill="#ff7718"/>
						  <text id="_" data-name="›" transform="translate(43 67)" fill="#fff" font-size="64" font-family="Poppins-SemiBold, Poppins" font-weight="600"><tspan x="-10.112" y="0">›</tspan></text>
						</svg>
					</button>

				</div>
			</div>



		</form>
	</div>



</div>
