<!--
 * button.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-12
 * Author: kaiseliger (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="container">
	<div class="row">
		<div class="col">
			<a href="#" class="button button--glitch"><span>.button.button--glitch</span></a>
		</div>
		<div class="col">
			<a href="#" class="button button--ghost">.button.button--ghost</a>
		</div>
	</div>
</div>
