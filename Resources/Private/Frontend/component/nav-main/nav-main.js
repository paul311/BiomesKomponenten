/**
 * nav-main.js
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de
 *
 * Date: 2017-07-24
 * Author: kaiseliger (*@short-cuts.de)
 * MIT License (MIT)
 */

Shorty.Component.Navmain = function($){

	var self = {
			settings: {},
			selectors: {},
			classes: {},
			elements: {}
		},
		_ = {};

	self.init = function(){
	};

	return self;

}(jQuery);

Shorty.Component.Navmain.init();
