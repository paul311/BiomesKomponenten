<!-- navigation:start -->
<nav class="nav-main navbar navbar-light container py-3 py-lg-4">
	<a class="navbar-brand" href="index.html">
		<img src="Resources/Public/img/clevermind-logo.png" alt="Logo: Clevermind">
	</a>
	<a href="#" class="nav-main__login btn btn-sm font-weight-light mr-3 ml-sm-auto p-0">
		login
	</a>
	<button class="nav-main__toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav pt-3">
			<li class="nav-item active">
				<a class="nav-link active" href="index.html">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="anmeldung.html">Anmeldung</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="kursinfo.html">Kursinfo</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="kursinfo-partner.html">Kursinfo Partner</a>
			</li>
		</ul>
	</div>
</nav>
<!-- navigation:end -->