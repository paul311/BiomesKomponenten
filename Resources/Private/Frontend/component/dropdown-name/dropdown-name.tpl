<!--
 * dorpdown-name.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-26
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="dropdown-name d-flex align-items-center">

	<div class="dropdown show d-flex">
		<a class="dropdown-toggle dropdown-name__header" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<h6 class="dropdown-name__header">Miriam Musetermann</h6>
		</a>

		<div class="dropdown-menu dropdown-name__test" aria-labelledby="dropdownMenuLink">
			<a class="dropdown-item  dropdown-name__test" href="#">Mein Profil</a>
			<a class="dropdown-item  dropdown-name__test" href="#">Account</a>
			<hr>
			<a class="dropdown-item" href="#">Logout</a>
		</div>
	</div>
	<svg class="" xmlns="http://www.w3.org/2000/svg" width="38" height="33.657" viewBox="0 0 38 33.657">
		<g id="Group_45" data-name="Group 45" transform="translate(-1375 -20)">
			<path id="Polygon_175" data-name="Polygon 175"
				d="M25.581,0a5,5,0,0,1,4.354,2.542l6.677,11.829a5,5,0,0,1,0,4.916L29.935,31.115a5,5,0,0,1-4.354,2.542H12.419a5,5,0,0,1-4.354-2.542L1.388,19.287a5,5,0,0,1,0-4.916L8.065,2.542A5,5,0,0,1,12.419,0Z" transform="translate(1375 20)"
				fill="#e3eff3" />
		</g>
	</svg>
	<svg class="dropdown-name__icon" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
		<path fill="#ff7719"
		  d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z">
		</path>
	</svg>

	<svg class="" xmlns="http://www.w3.org/2000/svg" width="38" height="33.657" viewBox="0 0 38 33.657">
		<g id="Group_45" data-name="Group 45" transform="translate(-1375 -20)">
			<path id="Polygon_175" data-name="Polygon 175"
			  d="M25.581,0a5,5,0,0,1,4.354,2.542l6.677,11.829a5,5,0,0,1,0,4.916L29.935,31.115a5,5,0,0,1-4.354,2.542H12.419a5,5,0,0,1-4.354-2.542L1.388,19.287a5,5,0,0,1,0-4.916L8.065,2.542A5,5,0,0,1,12.419,0Z" transform="translate(1375 20)"
			  fill="#e3eff3" />
		</g>
	</svg>
	<svg class="dropdown-name__icon" aria-hidden= "true" focusable="false" data-prefix="fas" data-icon="globe-americas" class="svg-inline--fa fa-globe-americas fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512">
		<path fill="#ff7719"
		  d="M248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm82.29 357.6c-3.9 3.88-7.99 7.95-11.31 11.28-2.99 3-5.1 6.7-6.17 10.71-1.51 5.66-2.73 11.38-4.77 16.87l-17.39 46.85c-13.76 3-28 4.69-42.65 4.69v-27.38c1.69-12.62-7.64-36.26-22.63-51.25-6-6-9.37-14.14-9.37-22.63v-32.01c0-11.64-6.27-22.34-16.46-27.97-14.37-7.95-34.81-19.06-48.81-26.11-11.48-5.78-22.1-13.14-31.65-21.75l-.8-.72a114.792 114.792 0 0 1-18.06-20.74c-9.38-13.77-24.66-36.42-34.59-51.14 20.47-45.5 57.36-82.04 103.2-101.89l24.01 12.01C203.48 89.74 216 82.01 216 70.11v-11.3c7.99-1.29 16.12-2.11 24.39-2.42l28.3 28.3c6.25 6.25 6.25 16.38 0 22.63L264 112l-10.34 10.34c-3.12 3.12-3.12 8.19 0 11.31l4.69 4.69c3.12 3.12 3.12 8.19 0 11.31l-8 8a8.008 8.008 0 0 1-5.66 2.34h-8.99c-2.08 0-4.08.81-5.58 2.27l-9.92 9.65a8.008 8.008 0 0 0-1.58 9.31l15.59 31.19c2.66 5.32-1.21 11.58-7.15 11.58h-5.64c-1.93 0-3.79-.7-5.24-1.96l-9.28-8.06a16.017 16.017 0 0 0-15.55-3.1l-31.17 10.39a11.95 11.95 0 0 0-8.17 11.34c0 4.53 2.56 8.66 6.61 10.69l11.08 5.54c9.41 4.71 19.79 7.16 30.31 7.16s22.59 27.29 32 32h66.75c8.49 0 16.62 3.37 22.63 9.37l13.69 13.69a30.503 30.503 0 0 1 8.93 21.57 46.536 46.536 0 0 1-13.72 32.98zM417 274.25c-5.79-1.45-10.84-5-14.15-9.97l-17.98-26.97a23.97 23.97 0 0 1 0-26.62l19.59-29.38c2.32-3.47 5.5-6.29 9.24-8.15l12.98-6.49C440.2 193.59 448 223.87 448 256c0 8.67-.74 17.16-1.82 25.54L417 274.25z">
		</path>
	</svg>






</div>
