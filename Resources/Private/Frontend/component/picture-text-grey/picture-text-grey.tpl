<!--
 * picture-text-grey.tpl
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de/
 *
 * Date: 2021-02-17
 * Author: paul (*@short-cuts.de)
 * MIT License (MIT)
-->
<div class="picture-text-grey">
	<div class="container">
		<div class="row d-flex align-items-center ">
			<div class="col-lg-6 offset-lg-0 offset-md-0 col-md-12 offset-xs-0 col-xs-11">
				<figure class="picture-text-grey__figure">
					<img src="Resources/Public/img/people-laughing.jpg" alt="Alles auf einem Blick">
				</figure>
			</div>
			<div class="col-lg-5 offset-md-1 col-md-11 offset-xs-1 col-xs-11">
				<div class="">
					<h2 class="picture-text-grey__header"><span>Sonderlösungen und große Volumina
						</span>
						Geschäftspartner*innen</h2>
					<p>
						<strong>
Heilpraktiker*innen – Ernährungsberater*innen – Fitnesscoaches – Fitnessstudios – Trainer*innen – Apotheken
						</strong>
					</p>
					<p class="">
						Sie möchten Ihren Klient*innen helfen, gesünder und leistungsfähiger zu werden?
Unsere DNA-basierten Darm-Mikrobiom-Tests unterstützen Sie dabei, Schwachstellen zu identifizieren und Ihre Beratung noch individueller zu machen. Unser INTEST.pro ist 100% evidenzbasiert und bietet Ihnen eine ausgezeichnete Grundlage, um Beschwerden zu analysieren und gezielte Empfehlungen auszusprechen. BIOMES bietet Ihnen dazu unschlagbare Konditionen.
					<p><a class="button--glitch button">Informieren sie sich</a>

					</p>
				</div>
			</div>
		</div>
	</div>
</div>
