{% extends 'layouts/default.tpl' %}

{% set pagetitle = 'Startseite | BIOMES' %}
{% set sc_styleguide = 'true' %}
{% set pagename = 'index' %}
{% set breadcrumbModifier = 'index' %}

{% block main %}

	<div class="ce ce--intro">
		{# intro.tpl #}
		{% include "../component/intro/intro.tpl" %}
	</div>

	<div class="ce ce--text-buttons">
		{# text-buttons.tpl #}
		{% include "../component/text-buttons/text-buttons.tpl" %}
	</div>

	<div class="ce ce--media">
		{# media.tpl #}
		{% include "../component/media/media.tpl" %}
	</div>

	<div class="ce ce--text-facts">
		{# text-facts.tpl #}
		{% include "../component/text-facts/text-facts.tpl" %}
	</div>

	<div class="ce ce--picture-text">
		{# picture-text.tpl #}
		{% include "../component/picture-text/picture-text.tpl" %}
	</div>

	<div class="ce ce--person">
		{# person.tpl #}
		{% include "../component/person/person.tpl" %}
	</div>

	<div class="ce ce--text-buttons">
		{# text-buttons.tpl #}
		{% include "../component/text-buttons/text-buttons.tpl" %}
	</div>

	<div class="ce ce--testimonials">
		{# testimonials.tpl #}
		{% include "../component/testimonials/testimonials.tpl" %}
	</div>


{% endblock %}
