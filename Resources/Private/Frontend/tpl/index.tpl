{% set pagetitle = 'Components' %}

{% set sc_styleguide = 'true' %}

{# head.tpl #}
{% include "./partials/head.tpl" %}

<body>

<div class="wrapper">

</div>
		{# header.tpl #}
		{% include "../component/header/header.tpl" %}
	</div>

			<div data-role="sc-sg" data-type="section" data-title="intro">
				{# intro.tpl #}
				{% include "../component/intro/intro.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="text-buttons">
				{# text-buttons.tpl #}
				{% include "../component/text-buttons/text-buttons.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="media">
				{# media.tpl #}
				{% include "../component/media/media.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="text-facts">
				{# text-facts.tpl #}
				{% include "../component/text-facts/text-facts.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="picture-text">
				{# picture-text.tpl #}
				{% include "../component/picture-text/picture-text.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="person">
				{# person.tpl #}
				{% include "../component/person/person.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="text-list">
				{# text-list.tpl #}
				{% include "../component/text-list/text-list.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="testimonials">
				{# testimonials.tpl #}
				{% include "../component/testimonials/testimonials.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="partner-form">
				{# partner-form.tpl #}
				{% include "../component/partner-form/partner-form.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="person">
				{# person.tpl #}
				{% include "../component/person/person.tpl" %}
			</div>


			<div data-role="sc-sg" data-type="section" data-title="partners">
				{# partners.tpl #}
				{% include "../component/partners/partners.tpl" %}
			</div>

	{# footer.tpl #}
	{% include "../component/footer/footer.tpl" %}

</div>

<script src="/Resources/Public/js/script.js"></script>
{% if sc_styleguide == 'true' %}
	<script src="/bower_components/sc-styleguide/private/js/sc_styleguide.js"></script>
{% endif %}
</body>

</html>
