<div class="row">
	<div class="lg-6 column">
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
	</div>
	<div class="lg-6 column">
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div>
</div>

<p>This is a paragraph. Paragraphs are preset with a font size, line height and spacing to match the overall vertical rhythm. To show what a paragraph looks like this needs a little more content so, did you know that there are storms occurring on Jupiter that are larger than the Earth? Pretty cool. Wrap strong around type to <strong>make it bold!</strong>. You can also use em to <em>italicize your words</em>.</p>
<p>Links are very standard, and the color is preset to the Foundation primary color. <a href="global.html">Learn more about Foundation's global colors.</a></p>

<hr>

<div class="row">
	<div class="lg-6 column">
		<ul>
			<li>List item with a much longer description or more content.</li>
			<li>List item</li>
			<li>List item
				<ul>
					<li>Nested list item</li>
					<li>Nested list item</li>
					<li>Nested list item</li>
				</ul>
			</li>
			<li>List item</li>
			<li>List item</li>
			<li>List item</li>
		</ul>
	</div>
	<div class="lg-6 column">
		<ol>
			<li>Cheese (essential)</li>
			<li>Pepperoni</li>
			<li>Bacon
				<ol>
					<li>Normal bacon</li>
					<li>Canadian bacon</li>
				</ol>
			</li>
			<li>Sausage</li>
			<li>Onions</li>
			<li>Mushrooms</li>
		</ol>
	</div>
</div>

<dl>
	<dt>Time</dt>
	<dd>The indefinite continued progress of existence and events in the past, present, and future regarded as a whole.</dd>
	<dt>Space</dt>
	<dd>A continuous area or expanse that is free, available, or unoccupied.</dd>
	<dd>The dimensions of height, depth, and width within which all things exist and move.</dd>
</dl>

<blockquote>
	Those people who think they know everything are a great annoyance to those of us who do.
	<cite>Isaac Asimov</cite>
</blockquote>