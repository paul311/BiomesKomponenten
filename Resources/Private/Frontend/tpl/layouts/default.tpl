{# head.tpl #}
{% include "../partials/head.tpl" %}

<body class="{{pagename}} bp-debug">

<div class="wrapper">
    {# header.tpl #}
    {% include "../../component/header/header.tpl" %}

	<main class="main">
		{{ block('main') }}
	</main>

	{# footer.tpl #}
	{% include "../../component/footer/footer.tpl" %}
</div>

<script src="Resources/Public/js/script.min.js"></script>
</body>

</html>
