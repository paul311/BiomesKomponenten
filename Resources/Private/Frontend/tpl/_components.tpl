{% set pagetitle = 'Components' %}
{% set sc_styleguide = 'true' %}

{# head.tpl #}
{% include "./partials/head.tpl" %}

<body>

<div class="wrapper">
	<div class="main">
		<div class="components">

			<div data-role="sc-sg" data-type="section" data-title="Colors">
				<div class="container">
					{# colors.tpl #}
					{% include "./partials/colors.tpl" %}
				</div>
			</div>

			<div data-role="sc-sg" data-type="section" data-title="Icons">
				<div class="container">
					{# icons.tpl #}
					{% include "./partials/icons.tpl" %}
				</div>
			</div>

			<div data-role="sc-sg" data-type="section" data-title="Grid">
				{# grid.tpl #}
				{% include "./partials/grid.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="Typography">
				<div class="container">
					{# typo.tpl #}
					{% include "./partials/typo.tpl" %}
				</div>
			</div>

		</div>
		{# header.tpl #}
		{% include "../component/header/header.tpl" %}
	</div>

			<div data-role="sc-sg" data-type="section" data-title="intro">
				{# intro.tpl #}
				{% include "../component/intro/intro.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="text-buttons">
				{# text-buttons.tpl #}
				{% include "../component/text-buttons/text-buttons.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="media">
				{# media.tpl #}
				{% include "../component/media/media.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="text-facts">
				{# text-facts.tpl #}
				{% include "../component/text-facts/text-facts.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="picture-text">
				{# picture-text.tpl #}
				{% include "../component/picture-text/picture-text.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="person">
				{# person.tpl #}
				{% include "../component/person/person.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="text-list">
				{# text-list.tpl #}
				{% include "../component/text-list/text-list.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="testimonials">
				{# testimonials.tpl #}
				{% include "../component/testimonials/testimonials.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="partner-form">
				{# partner-form.tpl #}
				{% include "../component/partner-form/partner-form.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="person">
				{# person.tpl #}
				{% include "../component/person/person.tpl" %}
			</div>

			<div data-role="sc-sg" data-type="section" data-title="partners">
				{# partners.tpl #}
				{% include "../component/partners/partners.tpl" %}
			</div>
			<div data-role="sc-sg" data-type="section" data-title="button">
					{# button.tpl #}
					{% include "../component/button/button.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="text-text-colored">
					{# text-text-colored.tpl #}
					{% include "../component/text-text-colored/text-text-colored.tpl" %}
				</div>



				<div data-role="sc-sg" data-type="section" data-title="text-picture">
					{# text-picture.tpl #}
					{% include "../component/text-picture/text-picture.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="picture-text-grey">
					{# picture-text-grey.tpl #}
					{% include "../component/picture-text-grey/picture-text-grey.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="text-hexagons">
					{# text-hexagons.tpl #}
					{% include "../component/text-hexagons/text-hexagons.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="text-orange">
					{# text-orange.tpl #}
					{% include "../component/text-orange/text-orange.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="product-checkboxes">
					{# product-checkboxes.tpl #}
					{% include "../component/product-checkboxes/product-checkboxes.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="side-menu">
					{# side-menu.tpl #}
					{% include "../component/side-menu/side-menu.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="person-small">
					{# person-small.tpl #}
					{% include "../component/person-small/person-small.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="new-features">
					{# new-features.tpl #}
					{% include "../component/new-features/new-features.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="email-download">
					{# email-download.tpl #}
					{% include "../component/email-download/email-download.tpl" %}
				</div>

				<div data-role="sc-sg" data-type="section" data-title="dropdown-name">
					{# dropdown-name.tpl #}
					{% include "../component/dropdown-name/dropdown-name.tpl" %}
				</div>

				<!-- <@newComponent@> -->

		</div>
	</div>
	{# footer.tpl #}
	{% include "../component/footer/footer.tpl" %}
</div>

	<script src="/Resources/Public/js/script.js"></script>
	{% if sc_styleguide == 'true' %}
	<script src="/bower_components/sc-styleguide/private/js/sc_styleguide.js"></script>
	{% endif %}
</body>

</html>
