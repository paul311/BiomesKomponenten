"use strict";

if(typeof Shorty == 'undefined'){
	var Shorty = {
		Component: {},
		Function: {},
		Handle: {},
		Util: {},
		Vars: {
			isIE: window.navigator.userAgent.indexOf('MSIE ') > 0 || // IE <= 10
					window.navigator.userAgent.indexOf('Trident/') > 0 || // IE 11
					window.navigator.userAgent.indexOf('Edge/') > 0  // IE 12
		},
		Elements: {}
	};
}

//$(document).foundation();
