/**
 * global.js
 *
 * Copyright SHORT CUTS GmbH
 * https://www.short-cuts.de
 *
 * Date: 2018-10-01
 * Author: undefined (*@short-cuts.de)
 * MIT License (MIT)
 */

Shorty.Component.Global = function($){

	var self = {
			settings: {},
			selectors: {
				panel: '.accordion [data-parent]:not(.show)'
			},
			classes: {
				hide: 'collapsed'
			},
			elements: {}
		},
		_ = {};

	self.init = function(){

		window.addEventListener('load', function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			var forms = document.getElementsByClassName('needs-validation');
			// Loop over them and prevent submission
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);

		$( self.selectors.panel ).prev().addClass( self.classes.hide )

	};

	return self;

}(jQuery);

Shorty.Component.Global.init();