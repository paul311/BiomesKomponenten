/**
 * Gulp project file
 *
 * Your description
 *
 * Copyright SHORT CUTS GmbH
 * http://www.short-cuts.de
 *
 * Date: 2016-12-31
 * Author: Kai Seliger (hello@yesjoar.com)
 * MIT License (MIT)
 */

var gulp = require('gulp'),
	glisting = require('gulp-task-listing'),
	gulpRequireTasks = require('gulp-require-tasks'),
	prompt = require('gulp-prompt'),
	runSequence = require('run-sequence');

global.config = require('./gulp/config');


/**
 *
 */
gulpRequireTasks({
	path: __dirname + '/gulp/tasks'
});

/**
 * The help task will show you a list of available tasks
 */
gulp.task('help', glisting);


/**
 * The project:init task is needed on the first project start.
 * It will get the project as you needed...
 */
gulp.task('project:init', function(){
	runSequence(
		'project:serve'
	)
});


/**
 * The project:serve task
 */
gulp.task('project:serve', function(){
	runSequence(
		[
			'compile:sass',
			'compile:js'
		],
		[
			'minify:js',
			'minify:css'
		],
		[
			'sassToHtml',
			'compile:twig'
		],
		[
			'browserSync',
			'watch'
		]
	)
});


/**
 * The project:deploy task will minify the assets (css, js)
 * and upload the whole project folder to your deployment server.
 *
 * Not needed source files like uncompiled and unminified scss and js files
 * will not be uploaded.
 */
gulp.task('project:deploy', function(){

});


/**
 * The project:deploy:static task is needed to deploy the
 * static files to a given static folder.
 *
 * You need do define a static folder ftp/fstp connection.
 *
 * TODO: We need the upload process via ftp/sftp.
 */
gulp.task('project:deploy:static', function(){
	runSequence(
		[
			'compile:sass',
			'compile:js',
			'compile:twig'
		],
		[
			'minify:css',
			'minify:js'
		],
		'copy:templatesToTmp',
		'copy:publicFolderToTmp'
	);
});


/**
 *
 */
gulp.task('project:deploy:dev', function(){

});


/**
 * The create:component task create a new component in
 * the component folder. At first you get asked if you
 * want to create a js file, too.
 */
gulp.task('create:component', function(){
	return gulp.src('/')
		.pipe(prompt.prompt({
			type: 'list',
			name: 'js',
			message: 'Create component with .js file?',
			choices: ['no', 'yes']
		}, function(res){
			if ( res.js === 'no' ) {
				runSequence(
					[
						'create:component:scss',
						'create:component:tpl',
						'replace:addNewComponentToSass',
						'replace:addNewComponentToSwig'
					],
					[
						'project:serve'
					]
				)
			} else if ( res.js === 'yes' ) {
				runSequence(
					[
						'create:component:scss',
						'create:component:tpl',
						'create:component:js',
						'replace:addNewComponentToSass',
						'replace:addNewComponentToSwig',
						'replace:addNewComponentToCompileJs'
					],
					[
						'project:serve'
					]
				)
			}
		}));
});

gulp.task('sassToHtml', function(){
	//gulp.start([
	//	'sassToHtml:colors',
	//	'sassToHtml:icons'
	//])
});


/**
 * The default task if you forgot to say which task should started.
 * It returns an error message and show you all abailable tasks.
 */
gulp.task('default', function(){
	console.log('Please choose one of the following tasks');
	gulp.start([
		'help'
	]);
});
