/**
 * The sass task rebuild the given sass files.
 * There's a screen.css file and standalone component css files
 */

var sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	browserSync = require('browser-sync'),
	sourcemaps = require('gulp-sourcemaps'),
	notify = require('gulp-notify');

module.exports = {
	fn: function(gulp){
		return gulp.src(global.config.src.sass)
			.pipe(sourcemaps.init())
			.pipe(sass({
				sourceComments: 'map',
				sourceMap: 'sass',
				outputStyle: 'nested',
				includePaths: [
					'bower_components/normalize-scss',
					'bower_components/bootstrap/scss',
                    'bower_components/font-awesome/web-fonts-with-css/scss'
				]
			}))
			.on('error', notify.onError({
				title: 'Sass Error',
				subtitle: '<%= error.relativePath %>:<%= error.line %>',
				message: '<%= error.formatted %>',
				open: 'file://<%= error.file %>',
				onLast: true
			}))
			.pipe(autoprefixer())
			.on('error', notify.onError({
				title: 'Sass Error',
				subtitle: '<%= error.relativePath %>:<%= error.line %>',
				message: '<%= error.formatted %>',
				open: 'file://<%= error.file %>',
				onLast: true
			}))
			.pipe(sourcemaps.write())
			.pipe(gulp.dest(global.config.public.css))
			.pipe(browserSync.reload({
				stream: true
			}));
	}
};
