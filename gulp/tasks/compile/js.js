var concat = require('gulp-concat'),
	browserSync = require('browser-sync'),
	sourcemaps = require('gulp-sourcemaps'),
	notify = require('gulp-notify');

module.exports = {
	fn: function(gulp){
		return gulp.src(global.config.src.js, {base: 'src'})
			.pipe(sourcemaps.init())
			.on('error', notify.onError({
				title: 'JS Error',
				message: '<%= error.message %>',
				onLast: true
			}))
			.pipe(concat('script.js'))
			.pipe(sourcemaps.write())
			.pipe(gulp.dest(global.config.public.js))
			.pipe(browserSync.reload({
				stream: true
			}));
	}
};
