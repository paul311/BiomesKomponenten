/**
 * The swig task get the .tpl files and rebuild it to .html files
 * Also it made the html file pretty.
 */

var twig = require('gulp-twig'),
	prettify = require('gulp-prettify'),
	browserSync = require('browser-sync'),
	notify = require('gulp-notify'),
	removeHtmlComments = require('gulp-remove-html-comments');

module.exports = function(gulp){
		return gulp.src(global.config.private.tpl + '/**.tpl')
			.pipe(twig())
			.on('error', notify.onError({
				title: 'Swig/Twig Error',
				subtitle: '<%= error.relativePath %>:<%= error.line %>',
				message: '<%= error.formatted %>',
				open: 'file://<%= error.file %>',
				onLast: true
			}))
			.pipe(removeHtmlComments())
			.pipe(prettify({
				indent_size: 4,
				indent_with_tabs: true
			}))
			.pipe(gulp.dest(global.config.private.templates))
			.pipe(browserSync.reload({
				stream: true
			}));

};