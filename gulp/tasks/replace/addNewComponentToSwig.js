var replace = require('gulp-replace-task');

module.exports = {
	fn: function(gulp){

		// get argument and set regexpression
		var argument = process.argv[3],
			regex = new RegExp("\--name="),
			date = new Date().toISOString().substring(0, 10);

		// check if syntax is correct
		if ( !regex.test(argument) ) {
			return console.log('Please use the following syntax: gulp create:component --name="your-component"');
		}

		// get component name
		var componentName = argument.replace(regex, ""),
			component = componentName;

		// build content string
		var str = "";
			str += "<div data-role=\"sc-sg\" data-type=\"section\" data-title=\"" + componentName + "\">\n";
			str += "					{# " + componentName + ".tpl #}\n";
			str += "					{% include \"..\/component\/" + componentName + "\/" + componentName + ".tpl\" %}\n";
			str += "				<\/div>\n";
			str += "\n";
			str += "				<!-- <@newComponent@> -->";


		return gulp.src(global.config.private.tpl + '/_components.tpl')
			.pipe(replace({
				patterns: [
					{
						match: /<!-- <@newComponent@> -->/g,
						replacement: str
					}
				]
			}))
			.pipe(gulp.dest(global.config.private.tpl));
	}
};