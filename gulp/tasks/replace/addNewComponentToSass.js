var replace = require('gulp-replace-task');

module.exports = {
	fn: function(gulp){

		// get argument and set regexpression
		var argument = process.argv[3],
			regex = new RegExp("\--name="),
			date = new Date().toISOString().substring(0, 10);

		// check if syntax is correct
		if ( !regex.test(argument) ) {
			return console.log('Please use the following syntax: gulp create:component --name="your-component"');
		}

		// get component name
		var componentName = argument.replace(regex, ""),
			component = componentName;

		// build content string
		var str = "";
			str += "\n";
			str += "@import \"..\/component\/" + componentName + "\/" + componentName + "\";\/\/ <@newComponent@>";

		return gulp.src(global.config.private.sass + '/screen.scss')
			.pipe(replace({
				patterns: [
					{
						match: /\/\/ <@newComponent@>/g,
						replacement: str
					}
				]
			}))
			.pipe(gulp.dest(global.config.private.sass));
	}
};