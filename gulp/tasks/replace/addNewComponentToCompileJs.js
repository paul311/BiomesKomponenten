var replace = require('gulp-replace-task');

module.exports = {
	fn: function(gulp){

		// get argument and set regexpression
		var argument = process.argv[3],
			regex = new RegExp("\--name=");

		// check if syntax is correct
		if ( !regex.test(argument) ) {
			return console.log('Please use the following syntax: gulp create:component --name="your-component"');
		}

		// get component name
		var componentName = argument.replace(regex, ""),
			component = componentName;

		// build content string
		var str = "";
			str += ",\n";
			str += "			\"" + global.config.private.component + "\/" + component + "\/" + component + ".js\"\/\/ <@newComponent@>";

		return gulp.src('gulp/config.js')
			.pipe(replace({
				patterns: [
					{
						match: /\/\/ <@newComponent@>/g,
						replacement: str
					}
				]
			}))
			.pipe(gulp.dest('gulp/'));
	}
};