/**
 * The watch task recognizes changes in the given files
 * and rebuild them after save
 *
 * @param gulp
 * @param callback
 */

module.exports = function(gulp){

	// watch template files
	gulp.watch([
		global.config.private.tpl + '/**/*.tpl',
		global.config.private.component + '/**/*.tpl'
	], ['compile:twig']);

	// watch styles
	gulp.watch([
		global.config.private.sass + '/**/*.scss',
		global.config.private.component + '/**/*.scss'
	], ['compile:sass']);

	// watch variables
	gulp.watch([
		global.config.private.sass + '/partials/_variables.scss'
	], ['sassToHtml']);

	// watch js
	gulp.watch([
		global.config.private.js + '/**/*.js',
		global.config.private.component + '/**/*.js'
	], ['compile:js'])

	// watch css
	gulp.watch(global.config.src.css, ['minify:css'])

	// watch scripts
	gulp.watch(global.config.src.script, ['minify:js'])

};