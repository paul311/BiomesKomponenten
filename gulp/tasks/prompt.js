var prompt = require('gulp-prompt');

module.exports = {
	fn: function(gulp){
		return gulp.src('/')//it may be anything
			.pipe(prompt.prompt({
				config: 'initSystem',
				type: 'list',
				name: 'system',
				message: 'Which system you want to initialize?',
				choices: ['patch', 'minor', 'major']
			}, function(res){
				gulp.start('replace:init');
			}));
	}
};