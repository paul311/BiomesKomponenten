/**
 * The browserSync task
 */

var browserSync = require('browser-sync');

module.exports = {
	fn: function(gulp){
		return browserSync({
			notify: false,
			server: {
				baseDir: global.config.private.templates,
				//index: '_components.html',
				routes: global.config.route
			}
		});
	}
};
