var fs = require("fs"),
	file = require('gulp-file'),
	gulpFn = require('gulp-fn');

module.exports = {
	fn: function(gulp) {
		return gulp.src(global.config.private.tpl + '/partials/colors.tpl')
			.pipe(gulpFn(function(file) {
				file.contents = Buffer.concat([
					file.contents = new Buffer( getHtml() )
				]);
			}))
			.pipe(gulp.dest(global.config.private.tpl + '/partials'));

		function getHtml() {
			var file = fs.readFileSync(global.config.private.sass + "/partials/_variables.scss", "utf-8"),
				regexBlock = /\/\*\s?<@colors*(\S*)(\n|\r|.)*?\s?colors@>\s?\*\//igm,
				regexItem = /\'([a-z0-9]-*)+\':\s?#[a-fA-F0-9]{3,6}/g,
				map = file.match(regexBlock)[0],
				map = map.match(regexItem),
				html = "";

			html += "<div class='row xs-up-1 sm-up-2 md-up-3 lg-up-4'>\n";

			for(var i = 0, len = map.length; i < len; i += 1) {

				string = map[i].replace(/\s/g, '');
				string = string.split(':');

				html += "<div class='column' style='margin-bottom:10px'>\n" +
					"	<div style='background:" + string[1] + ";padding:25px 0;border:1px solid #000'></div>\n" +
					"	<div>" + string[0] + ": " + string[1] + "</div>\n" +
					"</div>\n";

			}

			html += "</div>";

			return html;
		}
	}
};