var fs = require("fs"),
	file = require('gulp-file'),
	gulpFn = require('gulp-fn');

module.exports = {
	fn: function(gulp) {
		return gulp.src(global.config.private.tpl + '/partials/icons.tpl')
			.pipe(gulpFn(function(file) {
				file.contents = Buffer.concat([
					file.contents = new Buffer( getHtml() )
				]);
			}))
			.pipe(gulp.dest(global.config.private.tpl + '/partials'));

		function getHtml() {
			var file = fs.readFileSync(global.config.private.sass + "/partials/_variables.scss", "utf-8"),
				regexBlock = /\/\*\s?<@icons*(\S*)(\n|\r|.)*?\s?icons@>\s?\*\//igm,
				regexItem = /([a-zA-Z0-9]-*)+:\s?('|")\\[A-Za-z0-9]+('|")/g,
				map = file.match(regexBlock)[0],
				map = map.match(regexItem),
				html = "";

			html += "<div class='row xs-up-2 lg-up-6'>\n";

			for(var i = 0, len = map.length; i < len; i += 1) {

				string = map[i].replace(/\s/g, '');
				string = string.split(':');

				html += "<div class='column' style='margin-bottom:10px'>\n" +
					"	<div class='util-icon--before util-icon--" + string[0] + "' style='width:55px;height:55px;text-align:center;line-height:55px;border:1px solid #000'></div>\n" +
					"	<div>'" + string[0] + "': " + string[1] + "</div>\n" +
					"</div>\n";

			}

			html += "</div>";

			return html;
		}
	}
};