module.exports = {
	fn: function(gulp){
		return gulp.src([global.config.public.dir + '/**/*'])
			.pipe(gulp.dest('tmp/Resources/Public'));
	}
};