module.exports = {
	fn: function(gulp){
		return gulp.src(global.config.private.templates + '/*.html')
			.pipe(gulp.dest('tmp'));
	}
};