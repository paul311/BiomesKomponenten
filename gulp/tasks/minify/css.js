var cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename');

module.exports = {
	fn: function(gulp){
		return gulp.src(global.config.src.css)
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(cleanCSS({compatibility: 'ie8'}))
			.pipe(gulp.dest(global.config.public.css));
	}
};