var header = require('gulp-header'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	notify = require('gulp-notify'),
	banner = "/**\n" +
		" * SHORT CUTS GmbH\n" +
		" * http://www.short-cuts.de/\n" +
		" */\n";

module.exports = {
	fn: function(gulp){
		return gulp.src(global.config.src.script)
			.pipe(rename({
				suffix: '.min'
			}))
			.on('error', function(error){
				console.log(error.message);
				this.emit('end');
			})
			.pipe(uglify())
			.pipe(header(banner))
			.pipe(gulp.dest(global.config.public.js));
	}
};
