var file = require('gulp-file');

module.exports = {
	fn: function(gulp){

		// get argument and set regexpression
		var argument = process.argv[3],
			regex = new RegExp("\--name="),
			date = new Date().toISOString().substring(0, 10),
			name = process.env.USER;

		// check if syntaxt is correct
		if ( !regex.test(argument) ) {
			return console.log('Please use the following syntax: gulp create:component --name="your-component"');
		}

		// get component name
		var componentName = argument.replace(regex, ""),
			component = componentName;

		// build content string
		var str = "";
			str += "@charset \"utf-8\";\n";
			str += "\/**\n";
			str += " * " + component + ".scss\n";
			str += " *\n";
			str += " * Copyright SHORT CUTS GmbH\n";
			str += " * https:\/\/www.short-cuts.de\/\n";
			str += " *\n";
			str += " * Date: " + date + "\n";
			str += " * Author: " + name + " (*@short-cuts.de)\n";
			str += " * MIT License (MIT)\n";
			str += " *\/\n";
			str += "\n";
			str += "@import '..\/..\/sass\/partials\/functions';\n";
			str += "@import '..\/..\/sass\/partials\/variables';\n";
			str += "@import '..\/..\/sass\/partials\/mixins';\n";
			str += "\n";
			str += "$component: '." + component + "';\n";
			str += "\n";
			str += "\/\/ block\n";
			str += "#{$component} {\n";
			str += "}\n";
			str += "\n";
			str += "\/\/ element\n";
			str += "\n";
			str += "\n";
			str += "\/\/ modifier\n";
			str += "\n";

		// return file
		return file(global.config.private.component + '/' + component + '/' + component + '.scss', str, { src: true })
			.pipe(gulp.dest('./'));
	}
};