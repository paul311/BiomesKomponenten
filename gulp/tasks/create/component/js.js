var file = require('gulp-file');

module.exports = {
	fn: function(gulp){

		function pascalcase(str) {
			if (typeof str !== 'string') {
				throw new TypeError('expected a string.');
			}
			str = str.replace(/([A-Z])/g, ' $1');
			if (str.length === 1) { return str.toUpperCase(); }
			str = str.replace(/^[\W_]+|[\W_]+$/g, '').toLowerCase();
			str = str.charAt(0).toUpperCase() + str.slice(1);
			return str.replace(/[\W_]+(\w|$)/g, function (_, ch) {
				return ch.toUpperCase();
			});
		}

		// get argument and set regexpression
		var argument = process.argv[3],
			regex = new RegExp("\--name="),
			date = new Date().toISOString().substring(0, 10),
			name = process.env.USER;

		// check if syntax is correct
		if ( !regex.test(argument) ) {
			return console.log('Please use the following syntax: gulp create:component --name="your-component"');
		}

		// get component name
		var componentName = argument.replace(regex, ""),
			component = componentName,
			componentJS = pascalcase(component);

		// build content string
		var str = "";
			str += "\/**\n";
			str += " * " + component + ".js\n";
			str += " *\n";
			str += " * Copyright SHORT CUTS GmbH\n";
			str += " * https:\/\/www.short-cuts.de\n";
			str += " *\n";
			str += " * Date: " + date + "\n";
			str += " * Author: " + name + " (*@short-cuts.de)\n";
			str += " * MIT License (MIT)\n";
			str += " *\/\n";
			str += "\n";
			str += "Shorty.Component." + componentJS + " = function($){\n";
			str += "\n";
			str += "	var self = {\n";
			str += "			settings: {},\n";
			str += "			selectors: {},\n";
			str += "			classes: {},\n";
			str += "			elements: {}\n";
			str += "		},\n";
			str += "		_ = {};\n";
			str += "\n";
			str += "	self.init = function(){\n";
			str += "	};\n";
			str += "\n";
			str += "	return self;\n";
			str += "\n";
			str += "}(jQuery);\n";
			str += "\n";
			str += "Shorty.Component." + componentJS + ".init();";

		// return file
		return file(global.config.private.component + '/' + component + '/' + component + '.js', str, { src: true })
			.pipe(gulp.dest('./'));
	}
};