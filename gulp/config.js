/**
 *
 *
 */
 module.exports = {
 	"private": {
 		"dir": "Resources/Private/Frontend",
 		"component": "Resources/Private/Frontend/component",
 		"tpl": "Resources/Private/Frontend/tpl",
 		"templates": "Resources/Private/Frontend/templates",
 		"sass": "Resources/Private/Frontend/sass",
 		"js": "Resources/Private/Frontend/js"
 	},
 	"public": {
 		"dir": "Resources/Public",
 		"component": "Resources/Public/component",
 		"css": "Resources/Public/css",
 		"js": "Resources/Public/js"
 	},
 	"route": {
 		"/Resources": "Resources",
 		"/bower_components": "bower_components",
 		"/typo3conf/ext//Resources": "Resources"
 	},
 	"systems": {
 		"static": {

 		},
 		"dev": {

 		}
 	},
 	"src": {
 		"js": [
 			"bower_components/jquery/dist/jquery.min.js",
             "bower_components/bootstrap/dist/js/bootstrap.bundle.min.js",
 			"Resources/Private/Frontend/js/main.js",
 			"Resources/Private/Frontend/js/util/global.js",
 			"Resources/Private/Frontend/component/nav-main/nav-main.js"// <@newComponent@>
 	],
 		"sass": [
 			"Resources/Private/Frontend/sass/screen.scss"
 		],
 		"css": [
 			"Resources/Public/css/screen.css"
 		],
 		"script": [
 			"Resources/Public/js/script.js"
 		]
 	}
 };
